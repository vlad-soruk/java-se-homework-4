import java.util.Arrays;

public class Pet {
    private String species;
    private String nickname;
    private int age;
    private int tricklevel;
    private String[] habits;
    private Family family;

    Pet(){
        this.species = "undefined";
        this.nickname = "undefined";
    }
    Pet(String species, String nickname){
        this.species = species;
        this.nickname = nickname;
    }
    Pet(String species, String nickname, int age, int tricklevel, String[] habits){
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.tricklevel = tricklevel;
        this.habits = habits;
    }

    void eat(){
        System.out.println("\nI`m eating!");
    }

    void respond(){
        System.out.printf("\nHello owner. I`m %s. I missed you!", nickname);
    }

    void foul(){
        System.out.println("\nI should cover up the traces...");
    }

    public String getPetName(){
        return nickname;
    }

    public String getPetSpecies(){
        return species;
    }
    public int getPetAge(){
        return age;
    }
    public String getPetTrickLevel(){
        return tricklevel > 50 ? "very tricky" : "almost not tricky" ;
    }

    @Override
    public String toString() {
        return "\n%s{nickname='%s', age=%d, tricklevel=%d, habits=%s}".formatted(species, nickname, age, tricklevel, Arrays.toString(habits));
    }
}
