import java.util.Arrays;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;

    public Human getMotherInfo(){
        return mother;
    }
    public Human getFatherInfo(){
        return father;
    }
    public Human[] getChildrenInfo(){
        return children;
    }
    public Pet getPetInfo(){
        return pet;
    }

    Family(Human mother, Human father){
        this.mother = mother;
        this.father = father;
        this.children = new Human[0];
//        У батьків має встановлюватися посилання на поточну нову сім'ю
        this.mother.setFamily(this);
        this.father.setFamily(this);
        this.pet = (mother.getPet()!=null) ? mother.getPet() : (father.getPet()!=null) ? father.getPet() : null;
    }

    public boolean addChild(Human child){
        Human[] updatedChildrenArray = new Human[children.length + 1];
//        Копіюємо все, що знаходиться в children в updatedChildrenArray
        System.arraycopy(children, 0, updatedChildrenArray, 0, children.length);
        updatedChildrenArray[children.length] = child;
        child.setFamily(this);
        children = updatedChildrenArray;
        return true;
    }
    public boolean deleteChild(int index) {
        if (index < 0 || index >= children.length)
            return false;

        Human[] updatedChildrenArray = new Human[children.length - 1];
        for (int i = 0, j = 0; i < children.length; i++) {
            if (i != index) {
                updatedChildrenArray[j++] = children[i];
            }
        }

        children[index].setFamily(null);
        children = updatedChildrenArray;
        return true;
    }

    public int countFamily(){
        return 2 + children.length;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("\nFamily{");
        sb.append("mother=").append(mother.getName());
        sb.append(", father=").append(father.getName());
        sb.append(", children=").append(Arrays.toString(children));
        sb.append(", pet=");

        if (pet == null) {
            sb.append("No data for pet");
        }
        else {
            sb.append(pet.getPetName());
        }

        sb.append('}');
        return sb.toString();
    }
}
