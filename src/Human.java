import java.util.Arrays;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Pet pet;
    private Human mother;
    private Human father;
    private Family family;
//    Розклад позаробочих занять (schedule) (2-мірний масив: [день тижня] x [тип секції/відпочинку])
    private String[][] schedule;

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.mother = mother;
        this.father = father;
    }

    public Human(String name, String surname, int year, int iq, Pet pet, Human mother, Human father, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.pet = pet;
        this.mother = mother;
        this.father = father;
        this.schedule = schedule;
    }

    public Human() {

    }
    public String getName(){
        if (name==null){
            return "\"" + "Undefined name" + "\"";
        }
        return "\"" + name + " " + surname + "\"";

    }
    void greetPet(){
        System.out.printf("\nHello, %s!", pet.getPetName());
    }

    void describePet(){
        System.out.printf("\nI have a %s, it is %d years old, it is %s.", pet.getPetSpecies(), pet.getPetAge(), pet.getPetTrickLevel());
    }

    public Pet getPet(){
        return pet;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("\nHuman{");
        sb.append("name='").append(name).append('\'');
        sb.append(", surname='").append(surname).append('\'');
        sb.append(", year=").append(year);
        sb.append(", iq=").append(iq);
        sb.append(", pet=").append(pet.getPetName());
        sb.append(", mother=").append(mother.getName());
        sb.append(", father=").append(father.getName());
        sb.append(", schedule=[");
        for(int i = 0; i < schedule.length; i++){
            sb.append("[")
            .append(schedule[i][0])
            .append(", ")
            .append(schedule[i][1])
            .append("]");
            if(i <  schedule.length - 1) {
                sb.append(", ");
            }
        }
        sb.append(']');
        sb.append('}');
        return sb.toString();
    }

    public Family getFamily(){
        return family;
    }
    public void setFamily(Family family){
        this.family = family;
    }
}
