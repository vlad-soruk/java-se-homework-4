import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        String[] pet1Habits = {"eating", "sleeping", "running", "barking"};
        Pet pet1 = new Pet("dog", "Archi", 5, 52, pet1Habits);
        String[][] schedule = {
                {"Monday", "go to the cryptotrading courses"},
                {"Thursday", "go to the pool"},
                {"Friday", "go to the yoga"},
                {"Sunday", "go to the gym"},
        };
        System.out.println(pet1);
        Human mother1 = new Human("Katya", "Kozak", 44);
        Human father1 = new Human("Oleg", "Kozak", 50);
        Human human1 = new Human("Sasha", "Kozak", 20, 65, pet1, mother1, father1, schedule);
        System.out.println(human1);

        String[] pet2Habits = {"scratching", "jumping", "hunting"};
        Pet pet2 = new Pet("cat", "Ramos", 10, 10, pet2Habits);
        System.out.println(pet2);
        Human mother2 = new Human();
        Human father2 = new Human();

        Human human2 = new Human("Volodya", "Zaraz", 4, 55, pet2, mother1, father1, schedule);
        System.out.println(human2);

        Family family1 = new Family(mother1, father1);
        System.out.println(family1);

        family1.addChild(human1);
        System.out.println(family1);

        family1.addChild(human2);

        System.out.println(family1);

        Family human1Family = human1.getFamily();
        System.out.print("\nH u m a n  1  f a m i l y:");
        System.out.println(human1Family);

        int membersOfFamily1Count = family1.countFamily();
        System.out.printf("\nC o u n t  o f  f a m i l y : %s", membersOfFamily1Count);

        family1.deleteChild(0);
        System.out.println(family1);


//        human1.greetPet();
//        human1.describePet();
//        human2.greetPet();
//        human2.describePet();
    }

}